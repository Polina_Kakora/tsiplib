﻿namespace TSIP
{
    public struct IonosphereInfo
    {
        public float A0 { get; set; }
        public float A1 { get; set; }
        public float A2 { get; set; }
        public float A3 { get; set; }
        public float B0 { get; set; }
        public float B1 { get; set; }
        public float B2 { get; set; }
        public float B3 { get; set; }

        public IonosphereInfo(float a0, float a1, float a2, float a3, float b0, float b1, float b2, float b3)
        {
            A0 = a0;
            A1 = a1;
            A2 = a2;
            A3 = a3;
            B0 = b0;
            B1 = b1;
            B2 = b2;
            B3 = b3;
        }

    }
}
