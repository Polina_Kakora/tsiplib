﻿namespace TSIP
{
    public class ReceiverLocation
    {
        public double ReceiverLatitude { get; set; }
        public double ReceiverLongitude { get; set; }
        public double ReceiverAltitude { get; set; }

        public ReceiverLocation(double latitudedegrees, double longitudedegrees, double altitude)
        {
            ReceiverLatitude = latitudedegrees;
            ReceiverLongitude = longitudedegrees;
            ReceiverAltitude = altitude;
        }

        public ReceiverLocation()
        { }
    }
}
