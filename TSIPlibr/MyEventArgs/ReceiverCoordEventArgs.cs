﻿using System;

namespace TSIP
{
    public class ReceiverCoordEventArgs : EventArgs
    {
        public ReceiverLocation ReceiverCoord { get; set; }
        public byte AlmanacStatus { get; set; }

        public bool NoGpsTime { get; set; }

        public ReceiverCoordEventArgs(ReceiverLocation receiverLocation, byte almanacStatus, bool noGpsTime)
        {
            ReceiverCoord = receiverLocation;
            AlmanacStatus = almanacStatus;
            NoGpsTime = noGpsTime;
        }
    }
}