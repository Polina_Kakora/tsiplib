﻿using System;

namespace TSIP
{
    public class TimeEventArgs : EventArgs
    {
        public DateTime Currentdate { get; set; }

        public TimeEventArgs(DateTime currentdate)
        {
            Currentdate = currentdate;
        }
    }
}