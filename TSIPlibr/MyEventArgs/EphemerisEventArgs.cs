﻿using System;
using System.Collections.Generic;

namespace TSIP
{
    public class EphemerisEventArgs : EventArgs
    {
        public List<Ephemeris> Ephemeries { get; set; }

        public IonosphereInfo Ionosphere { get; set; }

        public UtcInfo Utc { get; set; }

        public EphemerisEventArgs(List<Ephemeris> ephemeries, IonosphereInfo ionosphere, UtcInfo utc)
        {
            Ephemeries = ephemeries;
            Ionosphere = ionosphere;
            Utc = utc;
        }
    }
}
